package ru.t1.rleonov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.repository.model.IProjectRepository;
import ru.t1.rleonov.tm.api.service.model.IProjectService;
import ru.t1.rleonov.tm.exception.field.NameEmptyException;
import ru.t1.rleonov.tm.exception.field.UserIdEmptyException;
import ru.t1.rleonov.tm.model.Project;
import ru.t1.rleonov.tm.model.User;
import ru.t1.rleonov.tm.repository.model.ProjectRepository;
import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository>
        implements IProjectService {

    @NotNull
    @Autowired
    public ProjectRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull Project project = new Project();
        project.setUser(entityManager.find(User.class, userId));
        project.setName(name);
        project.setDescription(description);
        repository.add(project);
        return project;
    }

}
