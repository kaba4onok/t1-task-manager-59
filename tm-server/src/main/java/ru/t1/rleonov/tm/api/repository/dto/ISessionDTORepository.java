package ru.t1.rleonov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IDTORepository<SessionDTO> {

    @Nullable
    SessionDTO findOneById(@NotNull String id);

}
