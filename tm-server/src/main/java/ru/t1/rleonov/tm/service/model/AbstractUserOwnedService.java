package ru.t1.rleonov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.rleonov.tm.api.service.model.IUserOwnedService;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.exception.entity.EntityNotFoundException;
import ru.t1.rleonov.tm.exception.field.IdEmptyException;
import ru.t1.rleonov.tm.exception.field.NameEmptyException;
import ru.t1.rleonov.tm.exception.field.StatusEmptyException;
import ru.t1.rleonov.tm.exception.field.UserIdEmptyException;
import ru.t1.rleonov.tm.model.AbstractUserOwnedModel;
import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel,
        R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R>
        implements IUserOwnedService<M> {

    @NotNull
    @Autowired
    protected IUserOwnedRepository<M> repository;

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId,
                           @Nullable final Sort sort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId, sort);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable M model = repository.findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        repository.update(model);
        return model;
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public M removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M model = repository.findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        repository.remove(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable M model = repository.findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        repository.update(model);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.clear();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void set(@NotNull final Collection<M> models) {
        if (models.isEmpty()) return;
        repository.set(models);
    }

}
