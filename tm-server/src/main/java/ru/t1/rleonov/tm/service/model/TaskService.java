package ru.t1.rleonov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.repository.model.ITaskRepository;
import ru.t1.rleonov.tm.api.service.model.ITaskService;
import ru.t1.rleonov.tm.exception.field.*;
import ru.t1.rleonov.tm.model.Task;
import ru.t1.rleonov.tm.model.User;
import ru.t1.rleonov.tm.repository.model.TaskRepository;
import javax.persistence.EntityManager;
import java.util.List;

@Service
@NoArgsConstructor
public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository>
        implements ITaskService {

    @NotNull
    @Autowired
    public TaskRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        task.setUser(entityManager.find(User.class, userId));
        task.setName(name);
        task.setDescription(description);
        repository.add(task);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByProjectId(userId, projectId);
    }

}
