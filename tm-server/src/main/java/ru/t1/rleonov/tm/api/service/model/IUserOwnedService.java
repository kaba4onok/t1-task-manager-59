package ru.t1.rleonov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.model.AbstractUserOwnedModel;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @NotNull
    M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    M removeById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    M changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    void clear(@Nullable String userId);

    void clear();

    @NotNull
    List<M> findAll(@Nullable String userId,
                    @Nullable Sort sort
    );

    @NotNull
    List<M> findAll();

}
