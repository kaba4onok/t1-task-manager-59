package ru.t1.rleonov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.model.AbstractUserOwnedModel;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    M findOneById(@NotNull String userId, @NotNull String id);

    public void clear(@Nullable String userId);

    public void clear();

    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    List<M> findAll();

}
