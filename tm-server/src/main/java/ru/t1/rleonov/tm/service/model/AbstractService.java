package ru.t1.rleonov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.repository.model.IRepository;
import ru.t1.rleonov.tm.api.service.model.IService;
import ru.t1.rleonov.tm.model.AbstractModel;
import javax.persistence.EntityManager;
import java.util.Collection;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>>
        implements IService<M> {

    @NotNull
    @Autowired
    protected IRepository<M> repository;

    @NotNull
    public EntityManager getEntityManager() {
        return repository.getEntityManager();
    }

    @Override
    @Transactional
    public void add(@NotNull final M model) {
        repository.add(model);
    }

    @Override
    @Transactional
    public void set(@NotNull final Collection<M> models) {
        repository.set(models);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        repository.update(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        repository.remove(model);
    }

}
