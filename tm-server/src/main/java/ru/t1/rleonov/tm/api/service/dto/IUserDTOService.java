package ru.t1.rleonov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.rleonov.tm.dto.model.UserDTO;

public interface IUserDTOService extends IDTOService<UserDTO>, IUserDTORepository {

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    UserDTO lockUserByLogin(@Nullable String login);

    @NotNull
    UserDTO unlockUserByLogin(@Nullable String login);

}
