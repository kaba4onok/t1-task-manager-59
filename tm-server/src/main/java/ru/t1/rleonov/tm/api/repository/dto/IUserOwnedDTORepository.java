package ru.t1.rleonov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.rleonov.tm.enumerated.Sort;
import java.util.List;

public interface IUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends IDTORepository<M> {

    M findOneById(@NotNull String userId, @NotNull String id);

    public void clear(@Nullable String userId);

    public void clear();

    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    List<M> findAll();

}
