package ru.t1.rleonov.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.rleonov.tm.api.service.dto.ISessionDTOService;
import ru.t1.rleonov.tm.exception.entity.SessionNotFoundException;
import ru.t1.rleonov.tm.exception.field.IdEmptyException;
import ru.t1.rleonov.tm.dto.model.SessionDTO;
import ru.t1.rleonov.tm.repository.dto.SessionDTORepository;

@Service
@NoArgsConstructor
public final class SessionDTOService extends AbstractDTOService<SessionDTO, ISessionDTORepository>
        implements ISessionDTOService {

    @NotNull
    @Autowired
    public SessionDTORepository repository;

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public SessionDTO create(@Nullable final SessionDTO session) {
        if (session == null) throw new SessionNotFoundException();
        repository.add(session);
        return session;
    }

    @Override
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public SessionDTO removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO session = findOneById(id);
        if (session == null) throw new SessionNotFoundException();
        repository.remove(session);
        return session;
    }

}
