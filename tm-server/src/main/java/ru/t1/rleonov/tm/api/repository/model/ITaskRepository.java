package ru.t1.rleonov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.model.Task;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@NotNull String userId, @NotNull Sort sort);

    @Nullable
    Task findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId);

    void clear();

    void clear(@NotNull String userId);

}
