package ru.t1.rleonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import java.util.ArrayList;
import java.util.List;
import static ru.t1.rleonov.tm.constant.UserTestData.*;
import static ru.t1.rleonov.tm.constant.ProjectTestData.*;

public final class TaskTestData {

    @NotNull
    public static final TaskDTO USER1_TASK1 = new TaskDTO("USER1_TASK1", "USER1_DESC1");

    @NotNull
    public static final TaskDTO USER1_TASK2 = new TaskDTO("USER1_TASK2", "USER1_DESC2");

    @NotNull
    public static final TaskDTO USER1_TASK3 = new TaskDTO("USER1_TASK3", "USER1_DESC3");

    @NotNull
    public static final TaskDTO USER2_TASK1 = new TaskDTO("USER2_TASK1", "USER2_DESC1");

    @NotNull
    public static final TaskDTO USER2_TASK2 = new TaskDTO("USER2_TASK2", "USER2_DESC2");

    @NotNull
    public static final TaskDTO ADMIN_TASK1 = new TaskDTO("ADMIN_TASK1", "ADMIN_DESC1");

    @NotNull
    public static final TaskDTO ADMIN_TASK2 = new TaskDTO("ADMIN_TASK2", "ADMIN_DESC2");

    @NotNull
    public static final TaskDTO WRONG_USERID_TASK = new TaskDTO("WRONG_USERID", "WRONG_USERID");

    @NotNull
    public static final List<TaskDTO> USER1_TASKS = new ArrayList<>();

    @NotNull
    public static final List<TaskDTO> USER1_PROJECT1_TASKS = new ArrayList<>();

    @NotNull
    public static final List<TaskDTO> USER2_TASKS = new ArrayList<>();

    @NotNull
    public static final List<TaskDTO> ADMIN_TASKS = new ArrayList<>();

    @NotNull
    public static final List<TaskDTO> ALL_TASKS = new ArrayList<>();

    static {
        USER1_TASKS.add(USER1_TASK1);
        USER1_TASKS.add(USER1_TASK2);
        USER2_TASKS.add(USER2_TASK1);
        USER2_TASKS.add(USER2_TASK2);
        ADMIN_TASKS.add(ADMIN_TASK1);
        ADMIN_TASKS.add(ADMIN_TASK2);
        USER1_PROJECT1_TASKS.add(USER1_TASK1);

        WRONG_USERID_TASK.setUserId("!!!WRONG_USERID!!!");
        USER1_TASK1.setStatus(Status.COMPLETED);

        ADMIN_TASK1.setProjectId(ADMIN_PROJECT1.getId());
        ADMIN_TASK2.setProjectId(ADMIN_PROJECT1.getId());

        USER1_PROJECT1_TASKS.forEach(task -> task.setUserId(USER1.getId()));
        USER1_TASKS.forEach(task -> task.setUserId(USER1.getId()));
        USER2_TASKS.forEach(task -> task.setUserId(USER2.getId()));
        ADMIN_TASKS.forEach(task -> task.setUserId(ADMIN.getId()));

        ALL_TASKS.addAll(USER1_TASKS);
        ALL_TASKS.addAll(USER2_TASKS);
        ALL_TASKS.addAll(ADMIN_TASKS);
    }

}
