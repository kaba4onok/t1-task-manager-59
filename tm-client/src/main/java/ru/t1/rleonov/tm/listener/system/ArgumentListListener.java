package ru.t1.rleonov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.api.model.IListener;
import ru.t1.rleonov.tm.event.ConsoleEvent;
import ru.t1.rleonov.tm.listener.AbstractListener;
import java.util.Collection;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @NotNull
    private static final String ARGUMENT = "-arg";

    @NotNull
    private static final String NAME = "arguments";

    @NotNull
    private static final String DESCRIPTION = "Show application arguments.";

    @Override
    @EventListener(condition = "@argumentListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ARGUMENTS]");
        for (@NotNull final AbstractListener listener : listeners) {
            @Nullable final String arg = listener.getArgument();
            if (arg == null || arg.isEmpty()) continue;
            System.out.println(arg);
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
